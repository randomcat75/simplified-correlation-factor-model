import numpy as np
from numba import vectorize, float64
from mpmath import *

def B89_f1(y):
    a1,a2,a3 = 1.5255251812009530,0.4576575543602858,0.4292036732051034
    cp = 0.7566445420735584 , -2.6363977871370960,5.4745159964232880,-12.657308127108290,4.1250584725121360,-30.425133957163840
    bp = 0.4771976183772063, -1.7799813494556270, 3.8433841862302150, -9.5912050880518490,2.1730180285916720, -30.4251338516036600
    yp = 1.,y**1,y**2,y**3,y**4,y**5
    cp = np.asarray(cp)
    bp = np.asarray(bp)
    yp = np.asarray(yp)
    P1 = np.dot(cp,yp)
    P2 = np.dot(bp,yp)
    g = -np.arctan(a1*y+a2) + a3
    return g*P1/P2

def gB89_f1(y):
    a1,a2,a3 = 1.5255251812009530,0.4576575543602858,0.4292036732051034
    cp = 0.7566445420735584 , -2.6363977871370960,5.4745159964232880,-12.657308127108290,4.1250584725121360,-30.425133957163840
    bp = 0.4771976183772063, -1.7799813494556270, 3.8433841862302150, -9.5912050880518490,2.1730180285916720, -30.4251338516036600
    yp = 1.,y**1,y**2,y**3,y**4,y**5
    dyp = 0., 1., 2.*y, 3.*y**2, 4.*y**3,5.*y**4
    cp = np.asarray(cp)
    bp = np.asarray(bp)
    yp = np.asarray(yp)
    P1 = np.dot(cp,yp)
    P2 = np.dot(bp,yp)
    g = -np.arctan(a1*y+a2) + a3
    dg = -a1/(1.+(a2+a1*y)**2)
    dP1 = np.dot(cp,dyp)
    dP2 = np.dot(bp,dyp)
    return dg*P1/P2 +g*(P2*dP1 - P1*dP2)/(P2**2)

def B89_f2(y):
    B  =  2.085749716493756
    dp = 0.00004435009886795587,0.58128653604457910000,66.7427645159406100000,434.267808972297700000,824.776576605223900000,1657.96527315821200000
    ep = 0.00003347285060926091,0.47917931023971350000,62.39226833857424000000,463.14816427938120000000,785.23603501040290000000,1657.96296822327300000000
    yp = 1.,y**1,y**2,y**3,y**4,y**5
    cp = np.asarray(dp)
    bp = np.asarray(ep)
    yp = np.asarray(yp)
    P1 = np.dot(dp,yp)
    P2 = np.dot(ep,yp)
    g = acsch(B*y) + 2.
    return g*P1/P2

def gB89_f2(y):
    B  =  2.085749716493756
    dp = 0.00004435009886795587,0.58128653604457910000,66.7427645159406100000,434.267808972297700000,824.776576605223900000,1657.96527315821200000
    ep = 0.00003347285060926091,0.47917931023971350000,62.39226833857424000000,463.14816427938120000000,785.23603501040290000000,1657.96296822327300000000
    yp = 1.,y**1,y**2,y**3,y**4,y**5
    dyp = 0., 1., 2.*y, 3.*y**2, 4.*y**3,5.*y**4
    cp = np.asarray(dp)
    bp = np.asarray(ep)
    yp = np.asarray(yp)
    P1 = np.dot(dp,yp)
    P2 = np.dot(ep,yp)
    g = acsch(B*y) + 2.
    dg = -1./(B* np.sqrt(1. + 1./(B**2*y**2))*y**2)
    dP1 = np.dot(dp,dyp)
    dP2 = np.dot(ep,dyp)
    return dg*P1/P2 +g*(P2*dP1 - P1*dP2)/(P2**2)

@vectorize([float64(float64)])
def B89_fit(y):
    if y <=0.:
        return B89_f1(y)
    else:
        return B89_f2(y)

def gB89_fit(y):
    if y <=0.:
        return gB89_f1(y)
    else:
        return gB89_f2(y)

def B89_param_y(y, rho):
    # Returns B89 parameters for a given value of curvature
    x = float(B89_fit(y))
    # parameters
    a = rho*8.*np.pi/(np.exp(-x))
    a = a**(1/3)
    b = x/a 
    c = a**3/(8.*np.pi)
    # reduced parameters
    return a,b,c

