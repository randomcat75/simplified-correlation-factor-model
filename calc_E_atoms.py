from modelXC import ModelXC
import numpy as np
import sys
from CFXm import CF

atoms={"H":1,"He":0,"Li":1,
        "Be":0,"B":1,
         "C":2,"N":3,
        "O":2,"F":1,"Ne":0,"Na":1,"Ne":0,"Na":1,
        "Mg":0,"Al":1,"Si":2,"P":3,"S":2,"Cl":1,
        "Ar":0}

def calc_E_cf(atom,positions,spin):
    """
    To calculate the total energies using the correlation factor model
    with converged pbe densities
    Input:
        atom:string
            atomic symbol
        positions:array
            positions of the atom
        spin:int
            the spin of the atom
    Return:
        model_dict:dict
            atom_name:energy
    """
    cf = CF(atom,positions,atoms[atom],approx='PBE,PBE',basis="cc-pvtz")
    E = cf.calc_Etot_cf()
    model_dict = {atom:E}
    print(model_dict)
    return model_dict


results = [calc_E_cf(atom,[[0,0,0]],atoms[atom]) for atom in atoms]

#to convert the list of dictionaries to a dictionary
results_dict = {}
for sub_dict in results:
    results_dict.update(sub_dict)

#creation of the file
f=open("E_atom.txt","w")
f.write("Atom "+'CFBRH'+"\n")
for atom in results_dict:
    f.write(atom +" %.8f\n"%results_dict[atom])
f.close()
