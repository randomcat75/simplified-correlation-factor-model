import numpy as np
from pyscf import gto, dft, lib
from pyscf.dft import numint
import sys
import re
from modelXC import ModelXC
from BR89_fit import B89_fit

def Pade54(x,a,b,c,d,e,f,g,h,i,j):
    return (a+b*x+c*x**2+d*x**3+e*x**4+f*x**5)/(1.+g*x+h*x**2+i*x**3+j*x**4)

def calc_A(rs,zeta):
        """
        Calculate A which is used to reproduce the on-top value of rho_xc of LSD
        Input:
            Rs:float
                wigner radius
            zeta:
                spin polarisation
        Return:
            A
        """
        mu=0.193
        nu=0.525
       #Calculate A (eq.38)
        return ((1-zeta**2)*0.5*(1.0+mu*rs)/(1.0+nu*rs+mu*nu*rs**2)-1.0)*(-2./(1.+zeta**2))

def calc_B(rs,zeta):
        """
        Calculate B which is used to reproduce the cusp value of rho_xc of LSD
        Input:
            Rs:float
                wigner radius
            zeta:
                spin polarisation
        Return:
            B
        """
        a=0.193
        b=0.525
        u=0.33825
        v=0.89679
        t=0.10134
        kappa=(4.0/(3.0*np.pi))*(9.0*np.pi/4.0)**(1.0/3.0)
        H = (1.0+u*rs)/(2.0+v*rs+t*rs**2)
        return (-4.0/(3.0*np.pi*kappa))*rs*H*((1.0-zeta**2)/(zeta**2+1.0))*((1.0+a*rs)/(1.0+b*rs+a*b*rs**2))

def symbD(A,B,moments, epsp):
        m1,m2,m3,m4,m5,m6 = moments
        return (A*m1*m4 - A*m2*m3 + B*m2*m4 - B*m3**2 - epsp*m4 - 3.*np.pi*m3/4.)/(m3*m6 - m4*m5)

def fitted_calc_rc_approx(rho_up, eps_x_up, rcp):
        """
        Calculate the reduced exchange hole curvature for one spin channel, associated with a reduced exchange energy density per particle. Using the Pade54 fit. 

        """
        rcp
        eps_up = 0.
        kf_up = (3.*np.pi**2*rho_up)**(1/3)
        eps_up = -eps_x_up*kf_up**2/(rho_up*2.*np.pi+1e-10)

        rc_up = Pade54(eps_up, *rcp)

        return  rc_up

def get_moments_JXBR(x):
       """
       Compute the 6 required moments for a given BR spin-hole. 

       """
      

       m1 = (np.exp(-2.*x/3.) *(3.*np.pi)**(2/3)* (-2. + 2.* np.exp(x) - x))/(4.*x)
       m2 = 3.*np.pi/4.
       m3 = (3.* np.exp(-4.*x/3.)* np.pi* (3.*np.pi)**(1/3) * (-4. + 4.*np.exp(x)-x+np.exp(x)*x**2))/(8.*x)
       m4 =  3./16.*3**(2/3)*np.exp(-2.*x/3.)*np.pi**(5./3.)*(12. + x**2)
       m5 = (9.*np.exp(-2.*x)* np.pi**2 * (-72. + 72.* np.exp(x) - 12.*x + 24.*np.exp(x)*x**2 + np.exp(x)*x**4))/(32.*x)
       m6 = (9.*np.exp(-x)* np.pi**2 * (180.* np.exp(-x/3.)* (3.*np.pi)**(1./3.)*x+20.*np.exp(-x/3.)* (3.*np.pi)**(1./3.)*x**3+ (1./2.)*np.exp(-x/3.)*(3.*np.pi)**(1./3.)*x**5))/(32.*x)
       return [-m1,-m2,-m3,-m4,-m5,-m6]


class CF(ModelXC):
    def __init__(self,molecule,positions,spin,approx='pbe,pbe',
                    basis='6-311+g2dp.nw',num_threads=1,ASE=False, charge=0, init_dm = None):
        super().__init__(molecule,positions,spin,approx,basis,num_threads,ASE, charge, init_dm)

 
        self.calc_eps_xc_post_approx('PBE,PBE')
        self.eps_x_PBE_up = self.eps_x_up
        self.eps_x_PBE_down = self.eps_x_down
        self.eps_c_PBE = self.eps_c
        self.eps_xc_PBE = (self.eps_x_PBE_up*self.rho_up+self.eps_x_PBE_down*self.rho_down)/self.rho_tot + self.eps_c_PBE


        self.rcp = np.asarray([-388.422467,1228.69964, -1693.51268,1263.07951,-501.892913, 83.3301243, 20.1927549, -32.3291364,14.2256469,-0.576957003])


    def calc_eps_xc_cf(self):

        A = calc_A(self.rs, self.zeta)
        B = calc_B(self.rs, self.zeta)


        #Calculate parameter D
        rc_up = fitted_calc_rc_approx(self.rho_up, self.eps_x_PBE_up, self.rcp)
        rc_down = fitted_calc_rc_approx(self.rho_down, self.eps_x_PBE_down, self.rcp)

        x_up= B89_fit(4./(3.**(5./3.)*np.pi**(2./3.)*rc_up))
        x_down = B89_fit(4./(3.**(5./3.)*np.pi**(2./3.)*rc_down))
           
        m_up = np.asarray(get_moments_JXBR(x_up))
        m_down = np.asarray(get_moments_JXBR(x_down))


        fu =  0.5*(1.+self.zeta)
        afu = [fu**(4./3.), fu, fu**(2./3.), fu**(1./3.), 1., fu**(-1./3.)]
        afu = np.asarray(afu)
        m_up = m_up*afu

        fd =  0.5*(1.-self.zeta+ 0.001)
        afd = fd**(4./3.), fd, fd**(2./3.), fd**(1./3.), 1., fd**(-1./3.)
        afd = np.asarray(afd)
        m_down = m_down*afd

        m = m_up+ m_down
  
        epsp = self.kf**2*self.eps_xc_PBE/(2.*np.pi*self.rho_tot) 

        D = symbD(A,B,m, epsp)
        sicA = 0.5+0.5*self.tauratio**2
        D=D*(1.- sicA)

         
        #Compute eps_xc


        kfa = (3.0*(np.pi**2.0) *self.rho_up)**(1.0/3.0)
        kfb = (3.0*(np.pi**2.0) *self.rho_down)**(1.0/3.0)

        rc_up = 2.*self.Q_up/(self.rho_up*kfa**2+1e-10)
        rc_up = 1./(4./(3.**(5./3.)*np.pi**(2./3.)*(rc_up*0.8+0.00001)))
        rc_up = 1./rc_up
        x_up = B89_fit(rc_up)

        rc_down = 2.*self.Q_down/(self.rho_down*kfb**2+1e-10)
        rc_down = 1./(4./(3.**(5./3.)*np.pi**(2./3.)*(rc_down*0.8+0.00001)))
        rc_down = 1./rc_down
        x_down = B89_fit(rc_down)


        m_up = np.asarray(get_moments_JXBR(x_up))
        m_down = np.asarray(get_moments_JXBR(x_down))

        m_up = m_up*afu
        m_down = m_down*afd

        m = m_up+ m_down

        m1,m2,m3,m4,m5,m6 = m

        C = -(3.*np.pi/4.+m2*A+m3*B+m6*D)/m4
        

        eps_xc = 2.*np.pi*self.rho_tot/self.kf**2*(A*m1+B*m2+C*m3+D*m5)



        return eps_xc

      
   
    def calc_Exc_cf(self):
        """
        To calculate the total XC energy.

        Return:
            Exc
        """
        exc= self.calc_eps_xc_cf()
        Exc = np.sum(self.weights*self.rho_tot*exc)
       
        return Exc

    def calc_Etot_cf(self):
        """
        To calculate the total energy

        Return
        """
        Exc = self.calc_Exc_cf()
        Etot = self.mf.e_tot-self.approx_Exc+Exc

        return Etot








